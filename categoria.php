<?php
include __DIR__.'/includes/config.php';

use es\ucm\fdi\aw\Producto;
use es\ucm\fdi\aw\Categoria;

$idCategoria = filter_input(INPUT_GET, 'idCategoria', FILTER_VALIDATE_INT);
if(!$idCategoria) {
    exit();
}
$categoria = Categoria::getCategoria($idCategoria);
$productos = Producto::getProductosCategoria($idCategoria);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Categoria <?= $categoria->getNombre(); ?></title>
    </head>
    <body>
        <h1>Productos</h1>
        <main>
<?php foreach($productos as $prod) { ?>
        <div>
            <div>Nombre: <?= $prod->getNombre(); ?></div>
            <div>Precio: <?= $prod->getPrecio(); ?></div>
        </div>
<?php } ?>
        </main>
    </body>
</html>