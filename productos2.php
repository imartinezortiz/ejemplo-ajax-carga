<?php
include __DIR__.'/includes/config.php';

use es\ucm\fdi\aw\Producto;
use es\ucm\fdi\aw\Categoria;

$idCategoria = filter_input(INPUT_GET, 'idCategoria', FILTER_VALIDATE_INT);
if(!$idCategoria) {
    exit();
}
$categoria = Categoria::getCategoria($idCategoria);
$productos = Producto::getProductosCategoria($idCategoria);

header('Content-type: application/json;charset=utf-8');

// NOTA: Ojo a la implementación de la interfaz JsonSerializable en Producto
echo json_encode($productos);
