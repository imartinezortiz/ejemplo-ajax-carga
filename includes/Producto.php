<?php

namespace es\ucm\fdi\aw;

class Producto implements \JsonSerializable
{
    private static $BD_PRODUCTOS;
    
    public static function inicializaBDSimulada()
    {
        self::$BD_PRODUCTOS = array(
            1 => new Producto(1, 'Producto 11', 1, 100.0)
            , 2 => new Producto(2, 'Producto 12', 1, 120.0)
            , 3 => new Producto(3, 'Producto 13', 1, 130.0)
            , 4 => new Producto(4, 'Producto 21', 2, 200.0)
            , 5 => new Producto(5, 'Producto 22', 2, 220.0)
            , 6 => new Producto(6, 'Producto 33', 3, 330.0)
            , 7 => new Producto(7, 'Producto 41', 4, 410.0)
        );
    }

    public static function getProductos()
    {
        return $BD_PRODUCTOS;
    }

    public static function getProducto(int $idProducto)
    {
        return $BD_PRODUCTOS[$idProducto];
    }

    public static function getProductosCategoria(int $idCategoria)
    {
        // Fuerza que se quede pensando la página
        sleep(5);
        $result = array();
        foreach(self::$BD_PRODUCTOS as $producto) {
            if ($producto->idCategoria == $idCategoria) {
                $result[] = $producto;
            }
        }
        return $result;
    }
    
    private $id;
    private $nombre;
    private $idCategoria;
    private $precio;

    private function __construct(int $id, string $nombre, int $idCategoria, float $precio)
    {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->idCategoria = $idCategoria;
        $this->precio = $precio;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre(string $nobmre)
    {
        $this->nombre = nombre;
    }

    public function getIdCategoria(){
        return $this->idCategoria;
    }

    public function getPrecio()
    {
        return $this->precio;
    }

    public function setPrecio(float $precio)
    {
        $this->precio = precio;
    }

    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
        $vars = get_object_vars($this);

        return $vars;
    }
}
Producto::inicializaBDSimulada();