<?php

namespace es\ucm\fdi\aw;

class Categoria
{
    private static $BD_CATEGORIAS;
    
    public static function inicializaBDSimulada()
    {
        self::$BD_CATEGORIAS = array(
            1 => new Categoria(1, 'Categoria 1')
            , 2 => new Categoria(2, 'Categoria 2')
            , 3 => new Categoria(3, 'Categoria 3')
            , 4 => new Categoria(4, 'Categoria 4')
        );
    }

    public static function getCategorias()
    {
        return self::$BD_CATEGORIAS;
    }

    public static function getCategoria(int $idCategoria)
    {
        return self::$BD_CATEGORIAS[$idCategoria];
    }
    
    private $id;
    private $nombre;

    private function __construct(int $id, string $nombre)
    {
        $this->id = $id;
        $this->nombre = $nombre;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre)
    {
        $this->nombre = $nombre;
    }
}
Categoria::inicializaBDSimulada();