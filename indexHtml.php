<?php
include __DIR__.'/includes/config.php';

use es\ucm\fdi\aw\Producto;
use es\ucm\fdi\aw\Categoria;

$categorias = Categoria::getCategorias();


?>
<!DOCTYPE html>
<html>
    <head>
        <title>Ejemplos AJAX</title>
    </head>
    <body>
    <h1>Categorias NO AJAX</h1>
        <nav>
            <ul>
<?php foreach($categorias as $cat) { ?>
                <li><a href="categoria.php?idCategoria=<?= $cat->getId(); ?>"><?= $cat->getNombre(); ?></a></li>
<?php } ?>
            </ul>
        </nav>
        <h1>Categorias AJAX</h1>
        <nav>
            <ul>
<?php foreach($categorias as $cat) { ?>
                <li><a class="categoria" href="categoria.php?idCategoria=<?= $cat->getId(); ?>"><?= $cat->getNombre(); ?></a></li>
<?php } ?>
            </ul>
        </nav>
        <main>
        </main>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="utils.js"></script>
        <script>
            // Convertimos todos los enlaces externos a enlaces internos
            $("a.categoria").each(function(index, element){
                let href = element.getAttribute('href');
                let search = getURLSearch(href);
                if (search != null) {
                    let idCategoria = parseParams(search).idCategoria;
                    if (idCategoria) {
                        element.setAttribute('href', 'indexHtml.php#idCategoria='+idCategoria);
                    }
                }
            });

            // Procesamos el evento asociado a visitar un enlace interno
            $( window ).on( 'hashchange', function( e ) {
                let params = getHashParams();
                let idCategoria = params.idCategoria;
                if (idCategoria) {
                    $("main").load("productos.php?idCategoria="+idCategoria);
                    $("main").append('<img width="50" src="cargando.gif" />');
                    productosMostrados = true;
                }
            } );
        </script>
    </body>
</html>
