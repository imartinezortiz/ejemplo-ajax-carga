<?php
include __DIR__.'/includes/config.php';

use es\ucm\fdi\aw\Producto;
use es\ucm\fdi\aw\Categoria;

$categorias = Categoria::getCategorias();


?>
<!DOCTYPE html>
<html>
    <head>
        <title>Ejemplos AJAX</title>
    </head>
    <body>
    <h1>Categorias NO AJAX</h1>
        <nav>
            <ul>
<?php foreach($categorias as $cat) { ?>
                <li><a href="categoria.php?idCategoria=<?= $cat->getId(); ?>"><?= $cat->getNombre(); ?></a></li>
<?php } ?>
            </ul>
        </nav>
        <h1>Categorias AJAX</h1>
        <nav>
            <ul>
<?php foreach($categorias as $cat) { ?>
                <li><a class="categoria" href="categoria.php?idCategoria=<?= $cat->getId(); ?>"><?= $cat->getNombre(); ?></a></li>
<?php } ?>
            </ul>
        </nav>
        <main>
        </main>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="utils.js"></script>
        <script>
            function loadProductosCategoria(idCategoria){
                $.get("productos2.php?idCategoria="+idCategoria, function(data, status) {
                    var html = '<h1>Productos</h1><ul>';
                    for(var idx = 0; idx < data.length; idx++) {
                        var p = data[idx];
                        html += '<li>Nombre: '+p.nombre+ ', Precio: '+p.precio+'</li>';
                    }
                    console.log(data);
                    html += '</ul>'
                    $("main").empty();
                    $("main").append(html);
                })
                .fail(function() {
                    console.log('Oops');
                });
                $("main").append('<img width="50" src="cargando.gif" />');
            }
            $("a.categoria").click(function(event){
                // Evitamos que se siga el enlace
                event.preventDefault();

                let element = event.target;
                let href = element.getAttribute('href');
                let search = getURLSearch(href);
                if (search != null) {
                    let idCategoria = parseParams(search).idCategoria;
                    if (idCategoria) {
                        // Cargamos los datos asíncronamente
                        loadProductosCategoria(idCategoria);

                        // Actualizamos la historia de navegación
                        var state = {idCategoria: idCategoria}
                        var title = ''; // no se utiliza
                        history.pushState(state, title, href);
                    }
                }
            });

            // Este evento se lanza cuando se navega por la historia con los botones de navegación
            $(window).on('popstate', function(e){
                var state = e.originalEvent.state;
                loadProductosCategoria(state.idCategoria);
            })
        </script>
    </body>
</html>