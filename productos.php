<?php
include __DIR__.'/includes/config.php';

use es\ucm\fdi\aw\Producto;
use es\ucm\fdi\aw\Categoria;

$idCategoria = filter_input(INPUT_GET, 'idCategoria', FILTER_VALIDATE_INT);
if(!$idCategoria) {
    exit();
}
$categoria = Categoria::getCategoria($idCategoria);
$productos = Producto::getProductosCategoria($idCategoria);
?>
<h1>Productos</h1>
<?php foreach($productos as $prod) { ?>
<div>
    <div>Nombre: <?= $prod->getNombre(); ?></div>
    <div>Precio: <?= $prod->getPrecio(); ?></div>
</div>
<?php } ?>