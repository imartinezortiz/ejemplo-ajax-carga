<!DOCTYPE html>
<html>
    <head>
        <title>Ejemplos AJAX</title>
    </head>
    <body>
    <h1>Ejemplos de carga de contenido con AJAX</h1>
    <p>Para simular una operación costosa, se simula un retardo de 5 cuando se van a listar los productos de una categoría.</p>
    <ol>
        <li><a href="indexHtml.php">Carga de contenido en formato HTML</a></li>
        <li><a href="indexJson.php">Carga de contenido en formato JSON</a></li>
    </ol>
    </body>
</html>